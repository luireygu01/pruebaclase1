		$(function(){
			$("[data-toggle='tooltip']").tooltip();
			$("[data-toggle='popover']").popover();
			$('.carousel').carousel({
				interval: 2000
			});
			$('#contacto').on('show.bs.modal', function(e){
				console.log('el contacto  se esta mostrando');
			});
			$('#contacto').on('shown.bs.modal', function(e){
				console.log('se mostro');
			});
			$('#contactoBtn').removeClass('btn-outline-success');
			$('#contactoBtn').addClass('btn warning');

			$('#contacto').on('hide.bs.modal', function(e){
				console.log('el modal contacto  se oculta');
			});
			$('#contacto').on('hidden.bs.modal', function(e){
				console.log('el modal contacto se oculto');
				$('#contactoBtn').prop('disabled', false);
			});
		});